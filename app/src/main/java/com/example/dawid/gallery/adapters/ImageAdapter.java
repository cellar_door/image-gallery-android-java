package com.example.dawid.gallery.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.dawid.gallery.R;


public class ImageAdapter extends BaseAdapter {

    private Context adapterContext;
    private int[] thumbsIds;
    private Activity adapterActivity;
    public ImageAdapter(Context ctx, int[] galleryImages, Activity activity){

        adapterContext = ctx;
        thumbsIds = galleryImages;
        adapterActivity = activity;
    }

    public int getCount(){
        return thumbsIds.length;
    }

    @Override
    public Object getItem(int position) {
        ImageView imageView = new ImageView(adapterContext);
        imageView.setImageResource(thumbsIds[position]);
        return imageView;
    }

    @Override
    public long getItemId(int position) {
        return thumbsIds[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GridView grid = (GridView) adapterActivity.findViewById(R.id.gridview);
        int desiredWidth = (int) Math.ceil(grid.getWidth() / grid.getNumColumns());
        int desiredHeight = (int) Math.ceil(grid.getHeight() / (grid.getNumColumns()*2));

        ImageView imageView = new ImageView(adapterContext);
        imageView.setLayoutParams(new GridView.LayoutParams(desiredWidth, desiredHeight));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(0, 0, 0, 0);

        imageView.setImageResource(thumbsIds[position]);
        return imageView;
    }

    public ImageView getCentralView(int position, int width, int height) {
        GridView grid = (GridView) adapterActivity.findViewById(R.id.gridview);
        ImageView imageView = new ImageView(adapterContext);
        imageView.setLayoutParams(new GridView.LayoutParams(width, height));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(0, 0, 0, 0);

        imageView.setImageResource(thumbsIds[position]);
        return imageView;
    }
}
