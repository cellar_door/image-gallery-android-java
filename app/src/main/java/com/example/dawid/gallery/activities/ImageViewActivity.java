package com.example.dawid.gallery.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dawid.gallery.R;
import com.example.dawid.gallery.adapters.CustomPagerAdapter;

public class ImageViewActivity extends AppCompatActivity {

    private int[] galleryImages;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        // Get the intent that started this activity
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        galleryImages = intent.getIntArrayExtra("thumbsIds");

        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(this, galleryImages);

        ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setCurrentItem(position);
    }
}
