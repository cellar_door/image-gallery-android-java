package com.example.dawid.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.dawid.gallery.activities.ImageViewActivity;
import com.example.dawid.gallery.adapters.ImageAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this, thumbsIds, this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), ImageViewActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("thumbsIds", thumbsIds);
                startActivity(intent);
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int firstItem = 0;
            private int lastItem = firstItem + 8;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    view.smoothScrollToPosition(firstItem);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                firstItem = firstVisibleItem;
                lastItem = firstVisibleItem + visibleItemCount;
            }
        });
    }

    private int[] thumbsIds = {
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_3, R.drawable.sample_6,
            R.drawable.sample_5, R.drawable.sample_7,
            R.drawable.sample_4, R.drawable.sample_2,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_3, R.drawable.sample_6,
            R.drawable.sample_5, R.drawable.sample_7,
            R.drawable.sample_4, R.drawable.sample_2
    };
}
